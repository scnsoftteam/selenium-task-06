// require('chromedriver');
import { $, browser, promise, ElementFinder} from 'protractor';

import { WaitUtils } from '../utils';

const http = require('http');

export default class GoogleWebPage {
  public html = $('html');
  public settingsLink = $('#fsettl');
  public searchSettingsLink = $('#fsett > [href*="/preferences"]');
  public regionName = $('._Vbu');

  public async get() {
    if (browser.logger) { browser.logger.trace(`I go to "http://google.com".`); }

    await browser.get('http://google.com/');

    // wait until the page is loaded
    try {
      await WaitUtils.waitForHttpsStatus(200);
      await WaitUtils.waitForTitleContains('Google');
    } catch (error) {
      fail(`Page not loaded.. ${error}`);
    }
  }

  public async openPreferences() {
    if (browser.logger) { browser.logger.trace(`I open to "Prefecences".`); }

    await WaitUtils.waitForReady(this.settingsLink);

    // click "Settings" link
    await this.settingsLink.click();

    // wait until the "Search settings" link is ready
    await WaitUtils.waitForReady(this.searchSettingsLink);

    // click "Search settings" link
    await this.searchSettingsLink.click();

    // wait until the URL of the page changes
    await WaitUtils.waitForURLContains('/preferences');
  }

  public async getLang() {
    let lang;

    // wait until the <html> is present
    await WaitUtils.waitForPresent(this.html);
    lang = await this.html.getAttribute('lang');

    return lang;
  }
}
