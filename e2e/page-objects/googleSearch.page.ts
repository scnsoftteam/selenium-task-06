import { $, $$, browser, Key } from 'protractor';
import { WaitUtils } from '../utils';

import GoogleWebPage from './googleWeb.page';

export default class GoogleSearchPage extends GoogleWebPage {
  public inputSearch = $('.gsfi[name="q"]');
  public pagination = $('#navcnt');
  public results = $$('.rc');

  public async search(value) {
    if (browser.logger) { browser.logger.trace(`I need search by "${value}".`); }

    // wait until the input search is ready
    await WaitUtils.waitForReady(this.inputSearch);

    // clear and add a value
    await this.inputSearch.clear();
    await this.inputSearch.sendKeys(value);

    // wait until the input value is change
    await WaitUtils.waitForValue(this.inputSearch, value);

    if (browser.logger) { browser.logger.trace(`I submit.`); }

    // and gooo!
    await this.inputSearch.sendKeys(Key.RETURN);

    // wait until the URL of the page changes
    await WaitUtils.waitForURLContains('/earch');

    // wait until the title of the page changes
    await WaitUtils.waitForTitleContains(`${value}`);
  }

  public async clickPage(pageNum: number) {
    if (browser.logger) { browser.logger.trace(`I click to page "${pageNum}".`); }

    let pageElm;

    // wait until the pagination is present
    await WaitUtils.waitForPresent(this.pagination);

    // wait until the pagination link is ready
    await WaitUtils.waitForReady(this.pagination.$(`[aria-label="Page ${pageNum}"]`));
    pageElm = await this.pagination.$(`[aria-label="Page ${pageNum}"]`);

    // click the pagination link
    await pageElm.click();

    // wait until the pagination link is stale
    await WaitUtils.waitForNotPresent(pageElm);
  }

  public async getInputSearch() {
    let value;

    // wait until the search input is present
    await WaitUtils.waitForPresent(this.inputSearch);
    value = await this.inputSearch.getAttribute('value');

    return value;
  }

  public async getCurrentPage() {
    let currentPage;

    // wait until the pagination is present
    await WaitUtils.waitForPresent(this.pagination);

    // wait until the current page is present
    await WaitUtils.waitForPresent(this.pagination.$('.cur'));
    currentPage = await this.pagination.$('.cur').getText();

    return currentPage;
  }

  public async hasResults() {
    let results;

    results = await this.results.count();

    return !!results;
  }
}
