import { $, browser, promise, until, By, ElementFinder, ExpectedConditions, ProtractorExpectedConditions } from 'protractor';
import { Deferred } from 'q';

const https = require('https');

export default class WaitUtils {
  public static DEFAULT_WAIT_TIMEOUT: number = 30000;
  public static EC: ProtractorExpectedConditions = ExpectedConditions;

  public static async waitForReady(elmFinder: ElementFinder) {
    await WaitUtils.waitForPresent(elmFinder);
    await WaitUtils.waitForClickable(elmFinder);
  }

  public static async waitForReCaptchaNotPresent() {
    await browser.wait(
      WaitUtils.EC.stalenessOf($('iframe[src*="/recaptcha"]')), 1000,
      'You have to check the reCAPTCHA!',
    );
  }

  public static async waitForPresent(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.presenceOf(elmFinder),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()} is not present.`,
    );
  }

  public static async waitForNotPresent(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.stalenessOf(elmFinder),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()}] is present but it should not be.`,
    );
  }

  public static async waitForClickable(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.elementToBeClickable(elmFinder),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()} is not ready to click on.`,
    );
  }

  public static async waitForVisible(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.visibilityOf(elmFinder),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()} is not visible.`,
    );
  }

  public static async waitForNotVisible(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.invisibilityOf(elmFinder),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()} is visible but it should not be.`,
    );
  }

  public static async waitForValue(elmFinder: ElementFinder, value: string, waitTimeout?: number) {
    const currentValue = await elmFinder.getAttribute('value');

    await browser.wait(
      WaitUtils.EC.textToBePresentInElementValue(elmFinder, value),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Element with locator ${elmFinder.locator()} value as "${currentValue}" - should be "${value}".`,
    );
  }

  // WARNING:
  // This function does't work correctly
  public static async waitForLocated(elmFinder: ElementFinder, waitTimeout?: number) {
    await browser.wait(
      until.elementLocated(elmFinder.locator()),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Still not able to locate element with locator "${elmFinder.locator()}" after maximum retries.`,
    );
  }

  public static async waitForAlertPresent(waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.alertIsPresent(),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      'Alert is not getting present.',
    );
  }

  public static async waitForTitle(title: string, waitTimeout?: number) {
    const currentTitle = await browser.getTitle();

    await browser.wait(
      WaitUtils.EC.titleIs(title),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Title page as "${currentTitle}" - should be "${title}".`,
    );
  }

  public static async waitForTitleContains(title: string, waitTimeout?: number) {
    const currentTitle = await browser.getTitle();

    await browser.wait(
      WaitUtils.EC.titleContains(title),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Title page as "${currentTitle}" - should be contain "${title}".`,
    );
  }

  public static async waitForURLContains(str: string, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.urlContains(str),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Expected URL "${str}" is not loaded.`,
    );
  }

  public static async waitForNotURLContains(str: string, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.not(WaitUtils.EC.urlContains(str)),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `URL "${str}" should not be there.`,
    );
  }

  public static async waitForURLEquals(str: string, waitTimeout?: number) {
    await browser.wait(
      WaitUtils.EC.urlIs(str),
      (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
      `Expected URL "${str}" is not loaded.`,
    );
  }

  public static async waitForHttpsStatus(statusCode: number, waitTimeout?: number) {
    const currentUrl = await browser.getCurrentUrl();
    const deferred = promise.defer();
    let res: any;

    try {
      res = await doGET(currentUrl);
      await browser.wait(
        () => res.statusCode === statusCode,
        (waitTimeout || WaitUtils.DEFAULT_WAIT_TIMEOUT),
        `HTTP status code as "${res.statusCode}" - should be "${statusCode}".`,
      );
    } catch (e) {
      browser.logger.error(`Oops..  ${e.message}.`);
      fail(e.message);
    }

    function doGET(url) {
      return new Promise ((resolve, reject) => {
        const req = https.get(url);

        req.on('response', (response) => {
          resolve(response);
        });

        req.on('error', (error) => {
          reject(error);
        });
      });
    }
  }
}
