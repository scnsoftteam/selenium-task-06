import { browser, Browser } from 'protractor';

import * as format from 'date-format';

export default class Helpers {
  public static async logger(category) {
    const caps = await browser.getCapabilities();

    switch (caps.get('browserName')) {
      case Browser.CHROME:
        const log = await browser.manage().logs().get(category);

        if (log.length > 0) {
          JSON.parse(JSON.stringify(log)).forEach((logger) => {
            browser.logger[`${category}Log`].info(`[${format('yyyy-MM-ddThh:mm:ss.SSS', new Date(logger.timestamp))}] [${logger.level}] - ${logger.message}`);
          });
        }
        break;
      case Browser.FIREFOX: /* not supported */ break;
    }
  }
}
