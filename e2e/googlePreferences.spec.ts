import { browser } from 'protractor';

import { GooglePreferencesPage, GoogleWebPage } from './page-objects';

import { Helpers, WaitUtils } from './utils';

import * as format from 'date-format';

// Protractor for Non-Angular application
browser.waitForAngularEnabled(false);

describe('Test 02: Google', () => {
  const googleWeb = new GoogleWebPage();
  const googlePreferences = new GooglePreferencesPage();

  beforeAll(async () => {
    await googleWeb.get();
  });

  afterEach(async () => {
    if (browser.logger) {
      await Helpers.logger('browser');
      await Helpers.logger('driver');
    }
  });

  it('should set language to English', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      await googleWeb.openPreferences();
      await googlePreferences.openLanguages();
      await googlePreferences.setLanguage('en');
      await googlePreferences.savePreferences();

      expect(await googleWeb.getLang()).toContain('en');
    } catch (error) {
      fail(error);
    }
  });

  it('should set region to Serbia', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      await googleWeb.openPreferences();
      await googlePreferences.setRegion('RS');
      await googlePreferences.savePreferences();

      expect(await googleWeb.getLang()).toContain('RS');
    } catch (error) {
      fail(error);
    }
  });

  it('should lang value `en-RS`', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      expect(await googleWeb.getLang()).toEqual('en-RS');
    } catch (error) {
      fail(error);
    }
  });
});
