'use strict';

exports.config = {
  SELENIUM_PROMISE_MANAGER: false,

  framework: 'jasmine',

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 900000,
    includeStackTrace: true,
    stopSpecOnFailure: true,
  },

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['--headless', '--disable-gpu'],
    },
  },

  // directConnect: true,

  specs: ['./**/*.spec.ts'],

  suites: {
    'test-01': 'e2e/googleSearch.spec.ts',
    'test-02': 'e2e/googlePreferences.spec.ts',
  },

  getPageTimeout: 600000,
  allScriptsTimeout: 600000,

  beforeLaunch() {
    require('ts-node').register({ project: '.' });
  },
};
