import { browser, Config } from 'protractor';

import { CustomLogger, CustomNetwork, CustomReporter } from './utils';

export let config: Config = {
  SELENIUM_PROMISE_MANAGER: false,

  framework: 'jasmine',

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 900000,
    includeStackTrace: true,
    stopSpecOnExpectationFailure: true,
  },

  seleniumAddress: 'http://localhost:4444/wd/hub',
  // directConnect: true,

  specs: ['./**/*.spec.js'],

  suites: {
    'test-01': 'e2e/googleSearch.spec.js',
    'test-02': 'e2e/googlePreferences.spec.js',
  },

  multiCapabilities: [
    {
      browserName: 'chrome',
      count: 1,
      loggingPrefs : { driver: 'ALL', browser: 'ALL' },
      chromeOptions: {
        args: ['--headless', '--disable-gpu', '--window-size=1024,768'],
      },
    },
    {
      'browserName': 'firefox',
      'count': 1,
      'marionette': true,
      'acceptInsecureCerts': true,
      'moz:firefoxOptions': {
        args: ['-headless'],
      },
    },
  ],

  getPageTimeout: 600000,
  allScriptsTimeout: 600000,

  async beforeLaunch() {
    await CustomLogger.clean();
  },

  async onPrepare() {
    // await CustomNetwork.throttleBrowser('GPRS'); // 'OFFLINE', 'GPRS', 'REGULAR_2G', 'GOOD_2G', 'REGULAR_3G', 'GOOD_3G'

    await CustomLogger.init();
    browser.logger = CustomLogger.get();
    browser.logger.browserLog = CustomLogger.get('browser');
    browser.logger.driverLog = CustomLogger.get('driver');

    CustomReporter.setup();
  },
};
