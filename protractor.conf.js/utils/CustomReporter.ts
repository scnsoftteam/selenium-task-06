import { browser } from 'protractor';

const JasmineConsoleReporter = require('jasmine-console-reporter');
const HtmlReporter = require('protractor-beautiful-reporter');

import CustomLogger from './CustomLogger';

export default class CustomReporter {
  public static setup() {
    jasmine.getEnv().clearReporters();

    jasmine.getEnv().addReporter(new JasmineConsoleReporter({
      colors: true,
      cleanStack: true,
      verbosity: true,
      listStyle: 'flat', // 'flat', 'indent'
      activity: false,
    }));

    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'dist/report',
      screenshotsSubfolder: 'images',
      jsonsSubfolder: 'jsons',
      gatherBrowserLogs: false,
      preserveDirectory: true,
    }).getJasmine2Reporter());

    jasmine.getEnv().addReporter({
      jasmineStarted(suiteInfo) {
        browser.logger.info(`Running suite(s) with total ${suiteInfo.totalSpecsDefined} specs.`);
      },
      suiteStarted(result) {
        browser.logger.info(`${result.fullName}`);
      },
      specStarted(result) {
        browser.logger.info(`#${result.id}: ${result.description}`);
      },
      specDone(result) {
        switch(result.status) {
          case 'passed':
            browser.logger.info(`#${result.id} passed.`);
            break;
          case 'failed':
            for (const error of result.failedExpectations) {
              browser.logger.error(`#${result.id} failed.\r\n\n${error.stack}\r\n\n`);
            }
            break;
        }
      },
      suiteDone(result) {
        switch(result.status) {
          case 'finished':
            browser.logger.info(`${result.fullName} passed.`);
            break;
          case 'failed':
            for (const error of result.failedExpectations) {
              browser.logger.error(`${result.fullName} failed.\r\n\n${error.stack}\r\n\n`);
            }
            break;
        }
      },
      jasmineDone() {
        browser.logger.info('Finished running all suite(s).');
      },
    });
  }
}
