import { existsSync, unlink } from 'fs';
import { merge } from 'lodash';
import { browser, Browser } from 'protractor';

import * as format from 'date-format';
import * as del from 'del';
import * as path from 'path';

const log4js = require('log4js');

export default class CustomLogger {
  public static async init() {
    const session = await browser.getSession();
    const caps = await browser.getCapabilities();
    const mainPath = `dist/logs/${caps.get('browserName')}/${session.getId()}`;
    let log4jsConf;

    log4jsConf = {
      appenders: {
        default: { type: 'file', filename: path.join(mainPath, 'default.log'), layout: { type: 'pattern', pattern: '[%r] [%p] %c - %m' } },
        log4js: { type: 'log4js-protractor-appender', layout: { type: 'pattern', pattern: '%n[%p] %c-log - %m' } },
      },
      categories: {
        default: { appenders: ['default'], level: 'ALL' },
      },
    };

    switch (caps.get('browserName')) {
      case Browser.CHROME:
        log4jsConf = merge(log4jsConf, {
          appenders: {
            browser: { type: 'file', filename: path.join(mainPath, 'browser.log'), layout: { type: 'pattern', pattern: '%m%n' } },
            driver: { type: 'file', filename: path.join(mainPath, 'driver.log'), layout: { type: 'pattern', pattern: '%m%n' } },
          },
          categories: {
            browser: { appenders: ['browser', 'log4js'], level: 'ALL'},
            driver: { appenders: ['driver'], level: 'ALL'},
          },
        });
        break;
      case Browser.FIREFOX: /* not supported */ break;
    }

    log4js.configure(log4jsConf);
  }

  public static async clean() {
    await del('dist/logs/**/*');
  }

  public static get(category?: string) {
    return log4js.getLogger(category);
  }
}
