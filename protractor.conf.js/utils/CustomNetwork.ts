import { browser, Browser, Command } from 'protractor';

export default class CustomNetwork {
  public static getNetworkSpec(net: string): ISpec {
    return NetworkSpec[net];
  }

  public static async throttleBrowser(net: string) {
    const caps = await browser.getCapabilities();

    switch (caps.get('browserName')) {
      case Browser.CHROME:
        await CustomNetwork.setNetworkConditions(CustomNetwork.getNetworkSpec(net));
        break;
      case Browser.FIREFOX: /* not supported */ break;
    }
  }

  public static async setNetworkConditions(spec: ISpec) {
    await browser.schedule(
      new Command('setNetworkConditions').setParameter('network_conditions', spec),
      `setNetworkConditions(${JSON.stringify(spec)})`,
    );
  }
}

interface INetworkSpec {
  OFFLINE: ISpec;
  GPRS: ISpec;
  REGULAR_2G: ISpec;
  GOOD_2G: ISpec;
  REGULAR_3G: ISpec;
  GOOD_3G: ISpec;
}

interface ISpec {
  offline: boolean;
  latency: number;
  download_throughput: number;
  upload_throughput: number;
}

export let NetworkSpec: INetworkSpec = {
  OFFLINE: {
    offline: true,
    latency: 0,
    download_throughput: 0,
    upload_throughput: 0,
  },
  GPRS: {
    offline: false,
    latency: 500,
    download_throughput: 50 * 1024,
    upload_throughput: 20 * 1024,
  },
  REGULAR_2G: {
    offline: false,
    latency: 300,
    download_throughput: 250 * 1024,
    upload_throughput: 50 * 1024,
  },
  GOOD_2G: {
    offline: false,
    latency: 150,
    download_throughput: 450 * 1024,
    upload_throughput: 150 * 1024,
  },
  REGULAR_3G: {
    offline: false,
    latency: 100,
    download_throughput: 750 * 1024,
    upload_throughput: 250 * 1024,
  },
  GOOD_3G: {
    offline: false,
    latency: 40,
    download_throughput: 1.5 * 1024 * 1024,
    upload_throughput: 750 * 1024,
  },
};
