# Selenium training

[![Build status](https://ci.appveyor.com/api/projects/status/vkncdl82vxd4eq1w?svg=true)](https://ci.appveyor.com/project/yakalinkin/selenium-task-06)

### Setup

```bash
npm install -g protractor
yarn install
```

```bash
yarn webdriver:update
```

### Run the test

```bash
yarn test --suite <your-test>
```

###### Example

```bash
yarn test --suite test-01
```
